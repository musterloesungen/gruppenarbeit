# Gruppenarbeit


#### Gruppe 1: Java Import Statement
- Erarbeiten Sie, wie das Import-Statement in Java funktioniert und welche unterschiedlichen Möglichkeiten es gibt, Klassen und Pakete zu importieren.
- Analysieren Sie die Auswirkungen von statischen und nicht-statischen Importen auf den Code.
- Präsentieren Sie, anhand von konkreten Code-Beispielen, die Verwendung und die Vorteile des Import-Statements in Java.

#### Gruppe 2: Vererbung in Java
- Untersuchen Sie den Mechanismus der Vererbung in Java und erklären Sie, wie Subklassen von Superklassen erben können.
- Erarbeiten Sie die Konzepte der Überladung und Überschreibung von Methoden im Kontext der Vererbung.
- Präsentieren Sie, mit Hilfe von Code-Beispielen, die Nutzung der Vererbung in Java und erläutern Sie die Vor- und Nachteile.

#### Gruppe 3: Container der Standardbibliothek
- Untersuchen Sie die verschiedenen Containerklassen der Java Standardbibliothek (z.B. Collections wie List, Set, Map).
- Erarbeiten Sie, wie diese Containerklassen verwendet werden und welche Vorteile sie in der Softwareentwicklung bieten.
- Präsentieren Sie anhand von Beispielen, wie die Containerklassen effektiv in einem Java-Programm eingesetzt werden können.

#### Gruppe 4: Multithreading
- Erarbeiten Sie die Grundlagen des Multithreading in Java und wie Threads erzeugt und gesteuert werden können.
- Untersuchen Sie die Synchronisation zwischen Threads und die Behandlung von Nebenläufigkeitsproblemen.
- Präsentieren Sie anhand von konkreten Code-Beispielen, wie Multithreading in Java effektiv umgesetzt werden kann.

#### Gruppe 5: Lambda Expressions
- Untersuchen Sie die Einführung und Verwendung von Lambda-Ausdrücken in Java.
- Erarbeiten Sie, wie Lambda-Ausdrücke zur Vereinfachung des Codes beitragen können und wie sie mit funktionalen Interfaces interagieren.
- Präsentieren Sie anhand von konkreten Code-Beispielen die Verwendung und Vorteile von Lambda-Ausdrücken in Java.
